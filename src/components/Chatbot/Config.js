import { createChatBotMessage } from "react-chatbot-kit";

const config = {
  botName: "bts_Bot",
  initialMessages: [createChatBotMessage(`Hello how can I help?`)],
};

export default config;
