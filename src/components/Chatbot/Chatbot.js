import React from "react";
import Chatbot from "react-chatbot-kit";

import config from "./Config";
import MessageParser from "./MessageParser";
import ActionProvider from "./ActionProvider";
import './Chatbot.css'
function Chat() {
  return (
    <div className="chatbot">
      <Chatbot
        config={config}
        messageParser={MessageParser}
        actionProvider={ActionProvider}
      />
    </div>
  );
}

export default Chat;