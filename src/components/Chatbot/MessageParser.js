class MessageParser {
    constructor(actionProvider, state) {
      this.actionProvider = actionProvider;
      this.state = state;
    }
  
    parse(message) {
      const lowMessage=message.toLowerCase()
      if(lowMessage.includes("hello")){
        this.actionProvider.greet()
      }
    }
  }
  
  export default MessageParser;