import React, { useState, createRef } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import "./Signup.css";
function Signup() {
  const usernameRef = createRef();
  const emailRef = createRef();
  //   const chatLink = (event) =>
  //     !username || !email ? event.preventDefault() : null;

  const signupUser = () => {
    const username = usernameRef.current.value;
    const email = emailRef.current.value;

    axios
      .post("http://localhost:5000/api/signup", {
        username,
        email,
      })
      .then((res) => {
        setId(res.data.result._id);
        // console.log(res.data.result._id);
      })
      .catch((err) => {
        alert(err.message);
      });
  };
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [id, setId] = useState("");
  return (
    <div className="signupOutterContainer">
      <div className="signupInnerContainer">
        <h1 className="heading">Chatbot Helpdesk</h1>
        <div>
          <input
            placeholder="Username"
            className="signupInput"
            type="text"
            onChange={(event) => setUsername(event.target.value)}
            ref={usernameRef}
          />
        </div>
        <div>
          <input
            placeholder="Email"
            className="signupInput"
            type="text"
            onChange={(event) => setEmail(event.target.value)}
            ref={emailRef}
          />
        </div>
        <Link onClick={signupUser} to={`/chatbot`}>
          <button className="button mt-20" type="submit">
            signup
          </button>
        </Link>
      </div>
    </div>
  );
}

export default Signup;
