import React from "react";
import {BrowserRouter as Router,Route} from 'react-router-dom'

import Signup from './components/Signup/Signup'
import Chatbot from './components/Chatbot/Chatbot'
function App() {
  return (
  <Router>
    <Route path="/" exact component={Signup} />
    <Route path="/chatbot" exact component ={Chatbot} />
  </Router>
  );
}

export default App;
